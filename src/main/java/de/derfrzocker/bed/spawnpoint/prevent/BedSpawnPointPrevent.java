package de.derfrzocker.bed.spawnpoint.prevent;

import org.bukkit.plugin.java.JavaPlugin;

import de.derfrzocker.bed.spawnpoint.prevent.listener.PlayerBedLeaveListener;
import lombok.Getter;

public class BedSpawnPointPrevent extends JavaPlugin {

	@Getter
	private volatile static BedSpawnPointPrevent instance;

	@Override
	public void onLoad() {
		instance = this;
	}

	@Override
	public void onEnable() {
		new PlayerBedLeaveListener();
	}

}
