package de.derfrzocker.bed.spawnpoint.prevent.utils;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import de.derfrzocker.bed.spawnpoint.prevent.BedSpawnPointPrevent;

public class Listeners implements Listener {

	public Listeners() {
		Bukkit.getServer().getPluginManager().registerEvents(this, BedSpawnPointPrevent.getInstance());
	}

}
