package de.derfrzocker.bed.spawnpoint.prevent.listener;

import static java.lang.String.format;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerBedLeaveEvent;

import de.derfrzocker.bed.spawnpoint.prevent.utils.Listeners;

public class PlayerBedLeaveListener extends Listeners {

	private final String PERMISSION_FORMAT = "bedspawnpoint.allow.%s";

	@EventHandler
	public void onPlayerBedLeave(PlayerBedLeaveEvent event) {

		if (!event.getPlayer().hasPermission(format(PERMISSION_FORMAT, event.getBed().getWorld().getName())))
			event.setSpawnLocation(false);

	}

}
